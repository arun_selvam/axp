import React, { Component } from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import './App.css';
// Styles
// CoreUI Icons Set
import '@coreui/icons/css/coreui-icons.min.css';
// Import Flag Icons Set
import 'flag-icon-css/css/flag-icon.min.css';
// Import Font Awesome Icons Set
import 'font-awesome/css/font-awesome.min.css';
// Import Simple Line Icons Set
import 'simple-line-icons/css/simple-line-icons.css';
// Import Main styles for this application
import './scss/style.css'

// Containers
import { DefaultLayout } from './containers';
// Pages
import { Login, Page404, Page500, Register } from './views/Pages';
import { db } from './firebase'
import { Campaignlist } from './views';
// import { renderRoutes } from 'react-router-config';



/* account.getCampaigns([Campaign.Fields.name], { limit: 100 })
.then((campaigns) => {
  if (campaigns.length >= 2 && campaigns.hasNext()) {
    return campaigns.next();
  } 
})
.then((campaigns) => {
  if (campaigns.hasNext() && campaigns.hasPrevious()) {
    return campaigns.previous();
  } else {
    Promise.reject(
      new Error('previous or next is not true')
    );
  }
  return campaigns.previous();
})
.catch((error) => {
}); */

/* 
const account = new AdAccount('act_460466184319079');
account
  .read([AdAccount.Fields.name, AdAccount.Fields.age])
  .then((account) => {
    console.log(account);
  })
  .catch((error) => {
  });

 */

class App extends Component {
  render() {
    return (
      <HashRouter>
        <Switch>
          <Route exact path="/login" name="Login Page" component={Login} />
          <Route exact path="/register" name="Register Page" component={Register} />
          <Route exact path="/404" name="Page 404" component={Page404} />
          <Route exact path="/500" name="Page 500" component={Page500} />
          <Route path="/" name="Home" component={DefaultLayout} />
        </Switch>
      </HashRouter>
    );
  }
}

export default App;
