import firebase from 'firebase'
require('firebase/firestore')


  var config = {
    apiKey: "AIzaSyBaNTK7VsnggLdwDF0iiGn22ZxG5yWrcPU",
    authDomain: "axpbrain.firebaseapp.com",
    databaseURL: "https://axpbrain.firebaseio.com",
    projectId: "axpbrain",
    storageBucket: "axpbrain.appspot.com",
    messagingSenderId: "1056149277932"
  };
  firebase.initializeApp(config);

export const db = firebase.firestore()
export const auth = firebase.auth
export const provider = new firebase.auth.FacebookAuthProvider()
export const fb = firebase
