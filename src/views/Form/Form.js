import React, { Component } from 'react';
import { Redirect } from 'react-router';
import * as util from 'util' // has no default export
import { inspect } from 'util' // or directly
import { Bar, Line } from 'react-chartjs-2';
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  FormGroup,
  Label,
  Input,
  Form,
  FormText,
  Progress,
  Row,
  Table,
  ListGroup,
  ListGroupItem,
  ListGroupItemHeading,
  ListGroupItemText
} from 'reactstrap';
import Widget03 from '../../views/Widgets/Widget03'
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities'

const brandPrimary = getStyle('--primary')
const brandSuccess = getStyle('--success')
const brandInfo = getStyle('--info')
const brandWarning = getStyle('--warning')
const brandDanger = getStyle('--danger')



const adsSdk = require('facebook-nodejs-business-sdk');
const accessToken = 'EAAEXwcTpG9wBAKe7W81jiT9rsU9ZBQbV7EOGtbtNZAQBRGadngwH8Dpc9bepqxWSOSW7sxSh9Jd4eCU4zxWHOEVVTiRInZBPxUI0tVZAFmGQTFEwazOqz5KnZCZB1OlsCWcGg92cTLRR44iU13JencsHnW9wpHnZCtLZCCjyVUP6nBxAO1hQLv4ZCWppr4A6TXOXFX09Qxqa02QZDZD';
const api = adsSdk.FacebookAdsApi.init(accessToken);
const AdAccount = adsSdk.AdAccount;
const Campaign = adsSdk.Campaign;
const account = new AdAccount('act_460466184319079');

let access_token = 'EAAEXwcTpG9wBAKe7W81jiT9rsU9ZBQbV7EOGtbtNZAQBRGadngwH8Dpc9bepqxWSOSW7sxSh9Jd4eCU4zxWHOEVVTiRInZBPxUI0tVZAFmGQTFEwazOqz5KnZCZB1OlsCWcGg92cTLRR44iU13JencsHnW9wpHnZCtLZCCjyVUP6nBxAO1hQLv4ZCWppr4A6TXOXFX09Qxqa02QZDZD';
let app_secret = '307595976448988';
let app_id = 'ce36cd8255594bacd3a192ba4c148cd0';


const showDebugingInfo = true; // Setting this to true shows more debugging info.
if (showDebugingInfo) {
  api.setDebug(true);
}


class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.onRadioBtnClick = this.onRadioBtnClick.bind(this);

    this.state = {
      dropdownOpen: false,
      radioSelected: 2,
      campaignList: [],
      RedirectToreview: false,
      campBudget: '0',
      campRequest: '-',
      campTarget: 'AXP Brain to decide',
    };
  }

  handleChange = (e) => {
    this.setState({
        [e.target.name]: e.target.value
    })
}


  async getCampaignList() {
    let campaignList = []
    let campaigns = await account.getCampaigns([Campaign.Fields.name], { limit: 2 });
    
    let abc = []
    campaigns.forEach(c => abc.push({ "name": c.name, "id": c.id }))
    console.log(abc);
    //const abc1 = [{"name":"My campaign","id":"23842824223830735"},{"name":"My campaign","id":"23842824223810735"}]; 

    campaignList = abc
      .map((d) => <li key={d.id}>{d.name}</li>);
    console.log(campaignList.length);
    console.log(campaignList[0].name);
    return campaignList
  };

  test() {
    const data = [{ "name": "My campaign", "id": "23842824223830735" }, { "name": "My campaign", "id": "23842824223810735" }];
    console.log(data);
    const listItems = data.map((d) => <li key={d.id}>{d.name}</li>);
    console.log(listItems.length);
    console.log(listItems[0].name);
    return listItems

  }


  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  }

  onRadioBtnClick(radioSelected) {
    this.setState({
      radioSelected: radioSelected,
    });
  }
  
  goToReview = (name, id, obj, optName, dailyBudget, start, end, request, target) => {
    this.setState({ RedirectToreview: true, campBudget: this.state.campBudget})
  }
  

  render() {
    if (this.state.RedirectToreview) {
      //return <Redirect push to={`/Form?campName=${this.state.campName}?campObj=${this.state.campObjective}?campID=${this.state.campID}?campStart=${this.state.campStart}`}  />
      return <Redirect to={{
        pathname: '/Review',
        search: '',
        state: { campBudget: this.state.campBudget, campName: this.props.location.state.campName, campID: this.props.location.state.campID, 
          campStart: this.state.campStart, campObjective: this.props.location.state.campObjective,
          optName: this.state.optName, campEnd: this.state.campEnd, campRequest: this.state.campRequest, campTarget: this.state.campTarget }
      }} />
    }
    return ( 
        <Card>
              <CardHeader>
                <strong>Optimisation Form</strong>
              </CardHeader>
              <CardBody>
                <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Campaign Name</Label>
                    </Col>
                    <Col xs="12" md="9">
                    <Label htmlFor="text-input">{this.props.location.state.campName}</Label>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Campaign ID</Label>
                    </Col>
                    <Col xs="12" md="9">
                    <Label htmlFor="text-input">{this.props.location.state.campID}</Label>
                    </Col>
                  </FormGroup>
                <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Campaign Objective</Label>
                    </Col>
                    <Col xs="12" md="9">
                    <Label htmlFor="text-input">{this.props.location.state.campObjective}</Label>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Optimisation Name</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" id="text-input" name="text-input" placeholder="FB Campaign A Optimisation" 
                        onChange={(evt) => { console.log(evt.target.value);
                        this.setState({optName: evt.target.value})
                        }}/>
                      <FormText color="muted">Optimisation Name</FormText>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="budget-input">Daily Budget</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" id="budget-input" name="budget-input" type="number" placeholder="Recommended min RM50 / day"  
                        onChange={(evt) => { console.log(evt.target.value);
                        this.setState({campBudget: evt.target.value})
                      }}/>
                      <FormText className="help-block">Daily Budget For Entire Campaign In Your Ad Account Currency</FormText>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="start-input">Start Date</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="date" id="start-input" name="start-input" placeholder="date"  
                        onChange={(evt) => { console.log(evt.target.value);
                        this.setState({campStart: evt.target.value})
                        if(evt.target.value > this.state.campEnd) {
                          console.log("Camp Start" + this.state.campStart);
                          console.log("Camp End" + this.state.campEnd); 
                          console.log("Date error");
                          document.getElementById("errorMsg").style.display = "inline";
                        }
                          else {
                            console.log("Camp Start" + this.state.campEnd);
                          console.log("Camp End" + this.state.campEnd); 
                            console.log("Date OK");
                            document.getElementById("errorMsg").style.display = "none";

                          }
                      }}/>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="end-input">End Date</Label><Label id="errorMsg"><strong>* Make sure end date is greater than start date</strong></Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="date" id="end-input" name="end-input" placeholder="date" 
                      onChange={(evt) => { console.log(evt.target.value);
                        this.setState({campEnd: evt.target.value})
                        if(evt.target.value < this.state.campStart) {
                          console.log("Camp Start" + this.state.campStart);
                          console.log("Camp End" + this.state.campEnd); 
                          console.log("Date error");
                          document.getElementById("errorMsg").style.display = "inline";
                          document.getElementById("submitBtn").style.display = "none";
                        }
                          else {
                            console.log("Camp Start" + this.state.campStart);
                          console.log("Camp End" + this.state.campEnd); 
                            console.log("Date OK");
                            document.getElementById("errorMsg").style.display = "none";
                            document.getElementById("submitBtn").style.display = "inline";

                          }
                        
                      }}/>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="textarea-input">Request</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="textarea" name="textarea-input" id="textarea-input" rows="9"
                             placeholder="Example: Target only male live in Kuala Lumpur with the age range between 25-50"
                             onChange={(evt) => { console.log(evt.target.value);
                              this.setState({campRequest: evt.target.value})
                            }} />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="kpi-input">Target KPI</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="number" id="kpi-input" name="kpi-input" placeholder=""
                      onChange={(evt) => { console.log(evt.target.value);
                        this.setState({campTarget: evt.target.value})
                      }}/>
                      <FormText className="help-block">This is calculated based on the past history of the campaign </FormText>
                    </Col>
                  </FormGroup>
                </Form>
              </CardBody>
              <CardFooter>
                <Button onClick={() => { this.goToReview() }} id="submitBtn" type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Submit</Button>
              </CardFooter>
            </Card>
    )
    
  }
}

export default Dashboard;
