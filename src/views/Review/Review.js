import React, { Component } from 'react';
import { Redirect } from 'react-router';

import * as util from 'util' // has no default export
import { inspect } from 'util' // or directly
import { Bar, Line } from 'react-chartjs-2';
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  FormGroup,
  Label,
  Input,
  Form,
  FormText,
  Progress,
  Row,
  Table,
  ListGroup,
  ListGroupItem,
  ListGroupItemHeading,
  ListGroupItemText
} from 'reactstrap';
import Widget03 from '../../views/Widgets/Widget03'
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities'
import { db } from '../../firebase'

const brandPrimary = getStyle('--primary')
const brandSuccess = getStyle('--success')
const brandInfo = getStyle('--info')
const brandWarning = getStyle('--warning')
const brandDanger = getStyle('--danger')

const adsSdk = require('facebook-nodejs-business-sdk');
const accessToken = 'EAAEXwcTpG9wBAKe7W81jiT9rsU9ZBQbV7EOGtbtNZAQBRGadngwH8Dpc9bepqxWSOSW7sxSh9Jd4eCU4zxWHOEVVTiRInZBPxUI0tVZAFmGQTFEwazOqz5KnZCZB1OlsCWcGg92cTLRR44iU13JencsHnW9wpHnZCtLZCCjyVUP6nBxAO1hQLv4ZCWppr4A6TXOXFX09Qxqa02QZDZD';
const api = adsSdk.FacebookAdsApi.init(accessToken);
const AdAccount = adsSdk.AdAccount;
const Campaign = adsSdk.Campaign;
const account = new AdAccount('act_460466184319079');

let access_token = 'EAAEXwcTpG9wBAKe7W81jiT9rsU9ZBQbV7EOGtbtNZAQBRGadngwH8Dpc9bepqxWSOSW7sxSh9Jd4eCU4zxWHOEVVTiRInZBPxUI0tVZAFmGQTFEwazOqz5KnZCZB1OlsCWcGg92cTLRR44iU13JencsHnW9wpHnZCtLZCCjyVUP6nBxAO1hQLv4ZCWppr4A6TXOXFX09Qxqa02QZDZD';
let app_secret = '307595976448988';
let app_id = 'ce36cd8255594bacd3a192ba4c148cd0';


const showDebugingInfo = true; // Setting this to true shows more debugging info.
if (showDebugingInfo) {
  api.setDebug(true);
}


class Review extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.onRadioBtnClick = this.onRadioBtnClick.bind(this);

    this.state = {
      dropdownOpen: false,
      radioSelected: 2,
      campaignList: [],
      RedirectToDashboard: false,
    };
  }

  async getCampaignList() {
    let campaignList = []
    let campaigns = await account.getCampaigns([Campaign.Fields.name], { limit: 2 });
    /* campaigns.forEach(c => campaignList.push(
       <p>{c.name}</p>
    )); */
    /* while (campaigns.hasNext()) {
        campaigns = await campaigns.next();
        //campaigns.forEach(c => console.log(c.name));
        campaigns.forEach(c => campaignList.push(
          <ListGroup>
                    <ListGroupItem active action>
                      <ListGroupItemHeading>{c.name}</ListGroupItemHeading>
                      <ListGroupItemText>
                        Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.
                      </ListGroupItemText>
                    </ListGroupItem>
                  </ListGroup>
        ))
    } */
    /* console.log(campaigns[0]["name"]);
    campaigns.forEach(c => console.log("mh " + c.name))
    for (int i = 0; i < campaigns.length; i++ ) {} */
    let abc = []
    campaigns.forEach(c => abc.push({ "name": c.name, "id": c.id }))
    console.log(abc);
    //const abc1 = [{"name":"My campaign","id":"23842824223830735"},{"name":"My campaign","id":"23842824223810735"}]; 

    campaignList = abc
      .map((d) => <li key={d.id}>{d.name}</li>);
    console.log(campaignList.length);
    console.log(campaignList[0].name);
    return campaignList
  };

  test() {
    const data = [{ "name": "My campaign", "id": "23842824223830735" }, { "name": "My campaign", "id": "23842824223810735" }];
    console.log(data);
    //const data =[{"name":"test1"},{"name":"test2"}];
    const listItems = data.map((d) => <li key={d.id}>{d.name}</li>);
    console.log(listItems.length);
    console.log(listItems[0].name);
    return listItems

  }


  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  }

  onRadioBtnClick(radioSelected) {
    this.setState({
      radioSelected: radioSelected,
    });
  }
  
  submitForm() {
    db.collection('accountAXP').add({CampaignName: this.props.location.state.campName, CampaignID: this.props.location.state.campID, CampaignObjective: this.props.location.state.campObjective, GroupName: this.props.location.state.optName, CampaignBudget: this.props.location.state.campBudget, CampaignStartDate: this.props.location.state.campStart, CampaignEndDate: this.props.location.state.campEnd, CampaignRequest: this.props.location.state.campRequest, CampaignTarget: this.props.location.state.campTarget })
    this.setState({
      RedirectToDashboard: true
    })
  }

  render() {
    if (this.state.RedirectToDashboard) {
      //return <Redirect push to={`/Form?campName=${this.state.campName}?campObj=${this.state.campObjective}?campID=${this.state.campID}?campStart=${this.state.campStart}`}  />
      return <Redirect to={{
        pathname: '/Dashboard',
        search: '',
      }} />
    }
    return ( 
        <Card>
              <CardHeader>
                <strong>Review</strong>
              </CardHeader>
              <CardBody>
                <Form action="" method="post" encType="multipart/form-data" className="form-horizontal" onSubmit={e => this.handleSubmit(e)}>
                <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Campaign Name</Label>
                    </Col>
                    <Col xs="12" md="9">
                     <Label htmlFor="text-input">{this.props.location.state.campName}</Label>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Campaign ID</Label>
                    </Col>
                    <Col xs="12" md="9">
                     <Label htmlFor="text-input">{this.props.location.state.campID}</Label>
                     </Col>
                  </FormGroup>
                <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Campaign Objective</Label>
                    </Col>
                    <Col xs="12" md="9">
                     <Label htmlFor="text-input">{this.props.location.state.campObjective}</Label>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Group Name</Label>
                    </Col>
                    <Col xs="12" md="9">
                    <Label htmlFor="text-input">{this.props.location.state.optName}</Label>
                      <FormText color="muted">Optimisation group name</FormText>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="budget-input">Daily Budget</Label>
                    </Col>
                    <Col xs="12" md="9">
                    <Label htmlFor="text-input">{this.props.location.state.campBudget}</Label>
                      <FormText className="help-block">Daily Budget For Entire Campaign In Your Ad Account Currency</FormText>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="start-input">Start Date</Label>
                    </Col>
                    <Col xs="12" md="9">
                    <Label htmlFor="text-input">{this.props.location.state.campStart}</Label>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="end-input">End Date</Label>
                    </Col>
                    <Col xs="12" md="9">
                    <Label htmlFor="text-input">{this.props.location.state.campEnd}</Label>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="textarea-input">Request</Label>
                    </Col>
                    <Col xs="12" md="9">
                    <Label htmlFor="text-input">{this.props.location.state.campRequest}</Label>

                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="kpi-input">Target KPI</Label>
                    </Col>
                    <Col xs="12" md="9">
                    <Label htmlFor="text-input">{this.props.location.state.campTarget}</Label>
                      <FormText className="help-block">This is calculated based on the past history of the campaign </FormText>
                    </Col>
                  </FormGroup>
                </Form>
              </CardBody>
              <CardFooter>
                <Button onClick={() => { this.submitForm() }} type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Submit</Button>
              </CardFooter>
            </Card>

    )
    
  }
}

export default Review;
