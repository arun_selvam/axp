import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Redirect } from 'react-router';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import firebase from '../../firebase';

import {
  BrowserRouter as Router,
  Link,
  Route,
  Switch,
} from 'react-router-dom';
import * as util from 'util' // has no default export
import { inspect } from 'util' // or directly
import { Bar, Line } from 'react-chartjs-2';
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Row,
  Table,
  ListGroup,
  ListGroupItem,
  ListGroupItemHeading,
  ListGroupItemText
} from 'reactstrap';
import Widget03 from '../../views/Widgets/Widget03'
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities'
import { db } from '../../firebase'

const brandPrimary = getStyle('--primary')
const brandSuccess = getStyle('--success')
const brandInfo = getStyle('--info')
const brandWarning = getStyle('--warning')
const brandDanger = getStyle('--danger')
const {
  signInWithEmail,
  signUpWithEmail,
  signInWithGoogle,
  signInWithFacebook,
  signInWithGithub,
  signInWithTwitter,
  googleAccessToken,
  facebookAccessToken,
  githubAccessToken,
  twitterAccessToken,
  twitterSecret,
  signOut,
  user,
  error 
} = this.props;
const { email, password } = this.state;



// Card Chart 1
const cardChartData1 = {
  labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
  datasets: [
    {
      label: 'My First dataset',
      backgroundColor: brandPrimary,
      borderColor: 'rgba(255,255,255,.55)',
      data: [65, 59, 84, 84, 51, 55, 40],
    },
  ],
};

const cardChartOpts1 = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips
  },
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        gridLines: {
          color: 'transparent',
          zeroLineColor: 'transparent',
        },
        ticks: {
          fontSize: 2,
          fontColor: 'transparent',
        },

      }],
    yAxes: [
      {
        display: false,
        ticks: {
          display: false,
          min: Math.min.apply(Math, cardChartData1.datasets[0].data) - 5,
          max: Math.max.apply(Math, cardChartData1.datasets[0].data) + 5,
        },
      }],
  },
  elements: {
    line: {
      borderWidth: 1,
    },
    point: {
      radius: 4,
      hitRadius: 10,
      hoverRadius: 4,
    },
  }
}


// Card Chart 2
const cardChartData2 = {
  labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
  datasets: [
    {
      label: 'My First dataset',
      backgroundColor: brandInfo,
      borderColor: 'rgba(255,255,255,.55)',
      data: [1, 18, 9, 17, 34, 22, 11],
    },
  ],
};

const cardChartOpts2 = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips
  },
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        gridLines: {
          color: 'transparent',
          zeroLineColor: 'transparent',
        },
        ticks: {
          fontSize: 2,
          fontColor: 'transparent',
        },

      }],
    yAxes: [
      {
        display: false,
        ticks: {
          display: false,
          min: Math.min.apply(Math, cardChartData2.datasets[0].data) - 5,
          max: Math.max.apply(Math, cardChartData2.datasets[0].data) + 5,
        },
      }],
  },
  elements: {
    line: {
      tension: 0.00001,
      borderWidth: 1,
    },
    point: {
      radius: 4,
      hitRadius: 10,
      hoverRadius: 4,
    },
  },
};

// Card Chart 3
const cardChartData3 = {
  labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
  datasets: [
    {
      label: 'My First dataset',
      backgroundColor: 'rgba(255,255,255,.2)',
      borderColor: 'rgba(255,255,255,.55)',
      data: [78, 81, 80, 45, 34, 12, 40],
    },
  ],
};

const cardChartOpts3 = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips
  },
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        display: false,
      }],
    yAxes: [
      {
        display: false,
      }],
  },
  elements: {
    line: {
      borderWidth: 2,
    },
    point: {
      radius: 0,
      hitRadius: 10,
      hoverRadius: 4,
    },
  },
};

// Card Chart 4
const cardChartData4 = {
  labels: ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
  datasets: [
    {
      label: 'My First dataset',
      backgroundColor: 'rgba(255,255,255,.3)',
      borderColor: 'transparent',
      data: [78, 81, 80, 45, 34, 12, 40, 75, 34, 89, 32, 68, 54, 72, 18, 98],
    },
  ],
};

const cardChartOpts4 = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips
  },
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        display: false,
        barPercentage: 0.6,
      }],
    yAxes: [
      {
        display: false,
      }],
  },
};

// Social Box Chart
const socialBoxData = [
  { data: [65, 59, 84, 84, 51, 55, 40], label: 'facebook' },
  { data: [1, 13, 9, 17, 34, 41, 38], label: 'twitter' },
  { data: [78, 81, 80, 45, 34, 12, 40], label: 'linkedin' },
  { data: [35, 23, 56, 22, 97, 23, 64], label: 'google' },
];

const makeSocialBoxData = (dataSetNo) => {
  const dataset = socialBoxData[dataSetNo];
  const data = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    datasets: [
      {
        backgroundColor: 'rgba(255,255,255,.1)',
        borderColor: 'rgba(255,255,255,.55)',
        pointHoverBackgroundColor: '#fff',
        borderWidth: 2,
        data: dataset.data,
        label: dataset.label,
      },
    ],
  };
  return () => data;
};

const socialChartOpts = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips
  },
  responsive: true,
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        display: false,
      }],
    yAxes: [
      {
        display: false,
      }],
  },
  elements: {
    point: {
      radius: 0,
      hitRadius: 10,
      hoverRadius: 4,
      hoverBorderWidth: 3,
    },
  },
};

// sparkline charts
const sparkLineChartData = [
  {
    data: [35, 23, 56, 22, 97, 23, 64],
    label: 'New Clients',
  },
  {
    data: [65, 59, 84, 84, 51, 55, 40],
    label: 'Recurring Clients',
  },
  {
    data: [35, 23, 56, 22, 97, 23, 64],
    label: 'Pageviews',
  },
  {
    data: [65, 59, 84, 84, 51, 55, 40],
    label: 'Organic',
  },
  {
    data: [78, 81, 80, 45, 34, 12, 40],
    label: 'CTR',
  },
  {
    data: [1, 13, 9, 17, 34, 41, 38],
    label: 'Bounce Rate',
  },
];

const makeSparkLineData = (dataSetNo, variant) => {
  const dataset = sparkLineChartData[dataSetNo];
  const data = {
    labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
    datasets: [
      {
        backgroundColor: 'transparent',
        borderColor: variant ? variant : '#c2cfd6',
        data: dataset.data,
        label: dataset.label,
      },
    ],
  };
  return () => data;
};

const sparklineChartOpts = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips
  },
  responsive: true,
  maintainAspectRatio: true,
  scales: {
    xAxes: [
      {
        display: false,
      }],
    yAxes: [
      {
        display: false,
      }],
  },
  elements: {
    line: {
      borderWidth: 2,
    },
    point: {
      radius: 0,
      hitRadius: 10,
      hoverRadius: 4,
      hoverBorderWidth: 3,
    },
  },
  legend: {
    display: false,
  },
};


const responseFacebook = (response) => {
  console.log(response.accessToken);
  const accessToken = response.accessToken;
}

const adsSdk = require('facebook-nodejs-business-sdk');
const accessToken = 'EAAEXwcTpG9wBAPM6ckEF3VJSBOo2c1hDAt174Tir1ZAfrHiFl5Rp5wEQpIdg1hbw8uiHB5f12i18W3PZCm6ceEQ1kpDlt4tkd8XsyZAg3GuJOVtURBExyVNA5pEm5GPRBxR6qk6dVvZAT7x2PoYgbqqLulZBQshadDrwqiR3gZChN9PZAGQVldcKkhk1qZCdmbRvNzaXQODsGVQZDZD';
const api = adsSdk.FacebookAdsApi.init(accessToken);
const AdAccount = adsSdk.AdAccount;
const Campaign = adsSdk.Campaign;
const account = new AdAccount('act_460466184319079');

let access_token = 'EAAEXwcTpG9wBAPM6ckEF3VJSBOo2c1hDAt174Tir1ZAfrHiFl5Rp5wEQpIdg1hbw8uiHB5f12i8W3PZCm6ceEQ1kpDlt4tkd8XsyZAg3GuJOVtURBExyVNA5pEm5GPRBxR6qk6dVvZAT7x2PoYgbqqLulZBQshadDrwqiR3gZChN9PZAGQVldcKkhk1qZCdmbRvNzaXQODsGVQZDZD';
let app_secret = '307595976448988';
let app_id = 'ce36cd8255594bacd3a192ba4c148cd0';


const showDebugingInfo = true; // Setting this to true shows more debugging info.
if (showDebugingInfo) {
  api.setDebug(true);
}

/* async function getCampaignList() {
   let campaignList = []
  let campaigns = await account.getCampaigns([Campaign.Fields.name], {limit: 20});
  campaigns.forEach(c => console.log(c.name));
  while (campaigns.hasNext()) {
      campaigns = await campaigns.next();
      campaigns.forEach(c => console.log(c.name));
      campaigns.forEach(c => campaignList.push(
        <ListGroup>
                  <ListGroupItem active action>
                    <ListGroupItemHeading>c.name</ListGroupItemHeading>
                    <ListGroupItemText>
                      Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.
                    </ListGroupItemText>
                  </ListGroupItem>
                </ListGroup>
      ))
  }
  return campaignList 
}; */

/* getAllCampaign = () => {
  let campaignList = []

} */


// Main Chart

//Random Numbers
function random(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

var elements = 27;
var data1 = [];
var data2 = [];
var data3 = [];

for (var i = 0; i <= elements; i++) {
  data1.push(random(50, 200));
  data2.push(random(80, 100));
  data3.push(65);
}

const mainChart = {
  labels: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'],
  datasets: [
    {
      label: 'My First dataset',
      backgroundColor: hexToRgba(brandInfo, 10),
      borderColor: brandInfo,
      pointHoverBackgroundColor: '#fff',
      borderWidth: 2,
      data: data1,
    },
    {
      label: 'My Second dataset',
      backgroundColor: 'transparent',
      borderColor: brandSuccess,
      pointHoverBackgroundColor: '#fff',
      borderWidth: 2,
      data: data2,
    },
    {
      label: 'My Third dataset',
      backgroundColor: 'transparent',
      borderColor: brandDanger,
      pointHoverBackgroundColor: '#fff',
      borderWidth: 1,
      borderDash: [8, 5],
      data: data3,
    },
  ],
};

const mainChartOpts = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips,
    intersect: true,
    mode: 'index',
    position: 'nearest',
    callbacks: {
      labelColor: function (tooltipItem, chart) {
        return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor }
      }
    }
  },
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        gridLines: {
          drawOnChartArea: false,
        },
      }],
    yAxes: [
      {
        ticks: {
          beginAtZero: true,
          maxTicksLimit: 5,
          stepSize: Math.ceil(250 / 5),
          max: 250,
        },
      }],
  },
  elements: {
    point: {
      radius: 0,
      hitRadius: 10,
      hoverRadius: 4,
      hoverBorderWidth: 3,
    },
  },
};

class Login extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.onRadioBtnClick = this.onRadioBtnClick.bind(this);

    this.state = {
      dropdownOpen: false,
      radioSelected: 2,
      campaignList: [],
      RedirectTocampaign: false,
      campName: '',
      campObj: '',
      campStart: '',
      campID: ''
    };
  }

  async getCampaignList() {
    let campaignList = []
    let campaigns = await account.getCampaigns([Campaign.Fields.name], { limit: 2 });
    /* campaigns.forEach(c => campaignList.push(
       <p>{c.name}</p>
    )); */
    /* while (campaigns.hasNext()) {
        campaigns = await campaigns.next();
        //campaigns.forEach(c => console.log(c.name));
        campaigns.forEach(c => campaignList.push(
          <ListGroup>
                    <ListGroupItem active action>
                      <ListGroupItemHeading>{c.name}</ListGroupItemHeading>
                      <ListGroupItemText>
                        Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.
                      </ListGroupItemText>
                    </ListGroupItem>
                  </ListGroup>
        ))
    } */
    /* console.log(campaigns[0]["name"]);
    campaigns.forEach(c => console.log("mh " + c.name))
    for (int i = 0; i < campaigns.length; i++ ) {} */
    let abc = []
    campaigns.forEach(c => abc.push({ "name": c.name, "id": c.id }))
    console.log(abc);
    //const abc1 = [{"name":"My campaign","id":"23842824223830735"},{"name":"My campaign","id":"23842824223810735"}]; 

    campaignList = abc
      .map((d) => <li key={d.id}>{d.name}</li>);
    console.log(campaignList.length);
    console.log(campaignList[0].name);
    return campaignList
  };

  test() {
    const data = [{ "name": "My campaign", "id": "23842824223830735" }, { "name": "My campaign", "id": "23842824223810735" }];
    console.log(data);
    //const data =[{"name":"test1"},{"name":"test2"}];
    const listItems = data.map((d) => <li key={d.id}>{d.name}</li>);
    console.log(listItems.length);
    console.log(listItems[0].name);
    return listItems

  }


  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  }

  onRadioBtnClick(radioSelected) {
    this.setState({
      radioSelected: radioSelected,
    });
  }



  async componentDidMount() {
    db
      .collection('accountAXP')
      .get()
      .then(collection => {
        const dbCampaigns = collection.docs.map(doc => doc.data().CampaignName)
        console.log(dbCampaigns)
        this.setState({ dbCampaigns })
      })

    let campaignList = []
    let campaigns = await account.getCampaigns([Campaign.Fields.name, Campaign.Fields.status, Campaign.Fields.objective, Campaign.Fields.start_time, Campaign.Fields.stop_time], { limit: 50 });
    campaigns.forEach(c => campaignList.push(
      <div key={c.id} onClick={() => { this.click(c.name, c.id, c.objective, c.start_time) }}>
        <ListGroup>
          <ListGroupItem>
            <ListGroupItemHeading>Campaign Name: {c.name} </ListGroupItemHeading>
            <ListGroupItemText>
              Campaign ID: {c.id} | Status: {c.status} | Objective: {c.objective} | Start: {c.start_time}  |
            </ListGroupItemText>

          </ListGroupItem>
        </ListGroup>
      </div>
    ))

    this.setState({ campaignList: campaignList })
    console.log(campaignList)

    let adAccountList = []
    const AdAccount = adsSdk.AdAccount;
    console.log("ad account " + adsSdk.AdACcount);
  }

  click = (name, id, obj, start) => {
    this.setState({ RedirectTocampaign: true, campID: id, campName: name, campObjective: obj, campStart: start })
  }



  render() {

    if (this.state.RedirectTocampaign) {
      //return <Redirect push to={`/Form?campName=${this.state.campName}?campObj=${this.state.campObjective}?campID=${this.state.campID}?campStart=${this.state.campStart}`}  />
      return <Redirect to={{
        pathname: '/Form',
        search: '',
        state: { campName: this.state.campName, campID: this.state.campID, campStart: this.state.campStart, campObjective: this.state.campObjective }
      }} />
    }
    return (

      <div className="animated fadeIn">
        <FacebookLogin
          appId="1088597931155576"
          autoLoad={true}
          fields="name,email,picture"
          scope="public_profile,user_friends,user_actions.books"
          callback={this.responseFacebook}
        />
        <form onSubmit={e => e.preventDefault()}>
          ...form input to take email and password for sign in
          <button
            type="submit"
            onClick={() => signInWithEmail(email, password)}
          >
            SignIn
          </button>
        </form>
        // For Sign Up
        <form onSubmit={e => e.preventDefault()}>
          ...form input to take email and password for sign up
          <button
            type="submit"
            onClick={() => signUpWithEmail(email, password)}
          >
            SignUp
          </button>
        </form>
        <button onClick={signInWithGoogle}>Signin with Google</button>
        <button onClick={signInWithFacebook}>Signin with Facebook</button>
        <button onClick={signInWithGithub}>Signin with Github</button>
        <button onClick={signInWithTwitter}>Signin with Twitter</button>
      </div>
      
    );
  }
}

export default Login;
