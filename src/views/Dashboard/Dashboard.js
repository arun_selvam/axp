import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Redirect } from 'react-router';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import firebase from 'firebase'

import {
  BrowserRouter as Router,
  Link,
  Route,
  Switch,
} from 'react-router-dom';
import * as util from 'util' // has no default export
import { inspect } from 'util' // or directly
import { Bar, Line } from 'react-chartjs-2';
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Row,
  Table,
  ListGroup,
  ListGroupItem,
  ListGroupItemHeading,
  ListGroupItemText
} from 'reactstrap';
import Widget03 from '../../views/Widgets/Widget03'
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities'
import { db } from '../../firebase'

const brandPrimary = getStyle('--primary')
const brandSuccess = getStyle('--success')
const brandInfo = getStyle('--info')
const brandWarning = getStyle('--warning')
const brandDanger = getStyle('--danger')







const cardChartData2 = {
  labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
  datasets: [
    {
      label: 'My First dataset',
      backgroundColor: brandInfo,
      borderColor: 'rgba(255,255,255,.55)',
      data: [1, 18, 9, 17, 34, 22, 11],
    },
  ],
};

const cardChartOpts2 = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips
  },
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        gridLines: {
          color: 'transparent',
          zeroLineColor: 'transparent',
        },
        ticks: {
          fontSize: 2,
          fontColor: 'transparent',
        },

      }],
    yAxes: [
      {
        display: false,
        ticks: {
          display: false,
          min: Math.min.apply(Math, cardChartData2.datasets[0].data) - 5,
          max: Math.max.apply(Math, cardChartData2.datasets[0].data) + 5,
        },
      }],
  },
  elements: {
    line: {
      tension: 0.00001,
      borderWidth: 1,
    },
    point: {
      radius: 4,
      hitRadius: 10,
      hoverRadius: 4,
    },
  },
};

class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.onRadioBtnClick = this.onRadioBtnClick.bind(this);

    this.state = {
      dropdownOpen: false,
      radioSelected: 2,
      campaignList: [],
      RedirectTocampaign: false,
      campName: '',
      campObj: '',
      campStart: '',
      campID: ''
    };
  }


  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  }

  onRadioBtnClick(radioSelected) {
    this.setState({
      radioSelected: radioSelected,
    });
  }



  async componentDidMount() {
    const adsSdk = require('facebook-nodejs-business-sdk');

    
    //const accessToken = this.props.location.state.accessToken;
    const accessToken = "EAAEXwcTpG9wBANfvcm08TMn8RyAaMrENdjMtbCuNhiZBKb4h4gvBmcj6IctbbdgYktTZAWGNHXrqrsoI5Fy6tMILXPLadcqrpzEcnuZC4LEUTIBU1u5YdbeOcoWxF4eHFmrRrgq9pKfyNGc1PFNhCqpnazWq6SppJjDqJg9erdtQn8SgiQX2yVfoAQiyHlJylmvP6t2ZAgZDZD"
    const api = adsSdk.FacebookAdsApi.init(accessToken);
    const AdAccount = adsSdk.AdAccount;
    const Campaign = adsSdk.Campaign;
    const account = new AdAccount('act_460466184319079');
    const showDebugingInfo = true; // Setting this to true shows more debugging info.
    if (showDebugingInfo) {
      api.setDebug(true);
    }
    db
      .collection('qiMlKmAVasUW93sI4TG7aTzqV5J3')
      .get()
      .then(collection => {
        const dbCampaigns = collection.docs.map(doc => doc.data().CampaignName)
        console.log(dbCampaigns)
        this.setState({ dbCampaigns })
      })

    let campaignList = []
    let campaigns = await account.getCampaigns([Campaign.Fields.name, Campaign.Fields.status, Campaign.Fields.objective, Campaign.Fields.start_time, Campaign.Fields.stop_time], { limit: 50 });
    campaigns.forEach(c => campaignList.push(
      <div key={c.id} onClick={() => { this.click(c.name, c.id, c.objective, c.start_time) }}>
        <ListGroup>
          <ListGroupItem>
            <ListGroupItemHeading>Campaign Name: {c.name} </ListGroupItemHeading>
            <ListGroupItemText>
              Campaign ID: {c.id} | Status: {c.status} | Objective: {c.objective} | Start: {c.start_time}  |
            </ListGroupItemText>

          </ListGroupItem>
        </ListGroup>
      </div>
    ))

    this.setState({ campaignList: campaignList })
    console.log(campaignList)

    /* let adAccountList = []
    const AdAccount = adsSdk.AdAccount;
    console.log("ad account " + adsSdk.AdACcount); */
  }

  click = (name, id, obj, start) => {
    this.setState({ RedirectTocampaign: true, campID: id, campName: name, campObjective: obj, campStart: start })
  }



  render() {
    if (this.state.RedirectTocampaign) {
      //return <Redirect push to={`/Form?campName=${this.state.campName}?campObj=${this.state.campObjective}?campID=${this.state.campID}?campStart=${this.state.campStart}`}  />
      return <Redirect to={{
        pathname: '/Form',
        search: '',
        state: { campName: this.state.campName, campID: this.state.campID, campStart: this.state.campStart, campObjective: this.state.campObjective }
      }} />
    }
    return (

      <div className="animated fadeIn">
       
        <Row>
          {this.state.dbCampaigns &&
            this.state.dbCampaigns.map((topic, index) =>
              <Col xs="12" sm="6" lg="3">
                <Card className="text-white bg-info">
                  <CardBody className="pb-0">
                    <ButtonGroup className="float-right">
                      <ButtonDropdown id='card4' isOpen={this.state.card4} toggle={() => { this.setState({ card4: !this.state.card4 }); }}>
                        <DropdownToggle caret className="p-0" color="transparent">
                          <i className="icon-settings"></i>
                        </DropdownToggle>
                        <DropdownMenu right>
                          <DropdownItem>Action</DropdownItem>
                          <DropdownItem>Another action</DropdownItem>
                          <DropdownItem disabled>Disabled action</DropdownItem>
                          <DropdownItem>Something else here</DropdownItem>
                        </DropdownMenu>
                      </ButtonDropdown>
                    </ButtonGroup>
                    <div key={index}>{topic}</div>
                  </CardBody>
                  <div className="chart-wrapper mx-3" style={{ height: '70px' }}>
                    <Line data={cardChartData2} options={cardChartOpts2} height={70} />
                  </div>
                </Card>
              </Col>
            )}
        </Row>
        
      </div>
    );
  }
}

export default Dashboard;
