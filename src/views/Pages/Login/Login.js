import React, { Component } from 'react';
import { Button, Card, CardBody, CardGroup, Col, Container, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import ReactDOM from 'react-dom';
import { Redirect } from 'react-router';
import { provider, auth, fb } from '../../../firebase';
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
import firebase from 'firebase'
import { db } from '../../../firebase'





class Login extends Component {

  uiConfig = {
    // Popup signin flow rather than redirect flow.
    signInFlow: 'popup',
    // We will display Google and Facebook as auth providers.
    signInOptions: [
      firebase.auth.FacebookAuthProvider.PROVIDER_ID,
    ],
    callbacks: {
      // Avoid redirects after sign-in.
      signInSuccessWithAuthResult: () => false
    }
  };



  constructor(props) {
    super(props);



    this.state = {
      RedirectToDashboard: false,
      accessToken: ''
    }
  }

  state = {
    user: null
  }

  login = () => {
    /*   auth.signInWithPopup(provider)
        .then(({ user }) => {
          this.setState({ user })
        }) */
  }

  logout = () => {
    auth.signOut().then(() => {
      this.setState({ user: null })
    })
  }

  componentDidMount = () => {
    firebase.auth().onAuthStateChanged(user => {
      this.setState({ isSignedIn: !!user })
      console.log("user", user)
      this.setState({user: user})

    })
  }


  responseFacebook = (response) => {
    this.setState({
      accessToken: response.accessToken,
      RedirectToDashboard: true,
      
    })
    //db.collection('accountAXP').add({CampaignName: this.props.location.state.campName, CampaignID: this.props.location.state.campID, CampaignObjective: this.props.location.state.campObjective, GroupName: this.props.location.state.optName, CampaignBudget: this.props.location.state.campBudget, CampaignStartDate: this.props.location.state.campStart, CampaignEndDate: this.props.location.state.campEnd, CampaignRequest: this.props.location.state.campRequest, CampaignTarget: this.props.location.state.campTarget })
    db.collection(this.state.user.uid).doc("accessToken").set({ accessToken: response.accessToken, displayName: this.state.user.displayName, email: this.state.user.email});


    console.log(response.accessToken)
    console.log(this.state.accessToken);
  }
  render() {
    const { user } = this.state
    if (this.state.RedirectToDashboard && this.state.accessToken != '') {
      return <Redirect to={{
        pathname: '/Dashboard',
        search: '',
        state: { accessToken: this.state.accessToken, abc: "abc" }
      }} />
    }

    if (this.state.isSignedIn) {
      return (
        <div className="app flex-row align-items-center">

          <Container>
            <Row className="justify-content-center">
              <Col md="8">
                <CardGroup>
                  <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: 44 + '%' }}>
                    <CardBody className="text-center">
                      <FacebookLogin
                        appId="307595976448988"
                        autoLoad
                        scope="ads_management"
                        callback={this.responseFacebook}
                        render={renderProps => (
                          <button onClick={renderProps.onClick}>Connect To Facebook</button>
                        )}
                      />
                    </CardBody>
                  </Card>
                </CardGroup>
              </Col>
            </Row>
          </Container>

        </div>
      )
      //return <Redirect push to={`/Form?campName=${this.state.campName}?campObj=${this.state.campObjective}?campID=${this.state.campID}?campStart=${this.state.campStart}`}  />
    }
    return (
      <div >

        <StyledFirebaseAuth
          uiConfig={this.uiConfig}
          firebaseAuth={firebase.auth()}
        />

      </div>
    )
  }
}

export default Login;
